.PHONY: build install run build-production run-production production

all: install build run

production: build-production run-production

install:
	@mvn install 

build:
	@mvn jfx:jar

build-production:
	@mvn -f pom.production.xml install

run:
	@java -jar target/jfx/app/dockerfx-1.0-SNAPSHOT-jfx.jar

run-production:
	@java -jar target/dockerfx-1.0-SNAPSHOT.jar
