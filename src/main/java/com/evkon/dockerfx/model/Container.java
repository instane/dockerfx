package com.evkon.dockerfx.model;

import com.github.dockerjava.api.model.ContainerPort;
import javafx.beans.property.*;
import javafx.collections.ObservableList;

public class Container {

    private final com.github.dockerjava.api.model.Container rawContainer;
    private final StringProperty id;
    private final StringProperty name;
    private final StringProperty image;
    private final StringProperty status;
    private final ListProperty<ContainerPort> ports;

    public Container(com.github.dockerjava.api.model.Container container) {
        this.rawContainer = container;
        this.name = new SimpleStringProperty(this.processRawNames(container.getNames()));
        this.id = new SimpleStringProperty(container.getId());
        this.image = new SimpleStringProperty(container.getImage());
        this.status = new SimpleStringProperty(container.getStatus());
        this.ports = new SimpleListProperty<ContainerPort>();

        for (ContainerPort port : container.getPorts()) {
            this.ports.add(port);
        }
    }

    @Override
    public String toString()
    {
        return String.format("%s (%s) %s", name.getValue(), image.getValue(), status.getValue());
    }

    private String processRawNames(String[] names)
    {
        StringBuilder name = new StringBuilder();
        for (String _name : names) {
            name.append(_name);
        }
        if (name.charAt(0) == '/') {
            name.deleteCharAt(0);
        }
        return name.toString();
    }

    public com.github.dockerjava.api.model.Container getRawContainer() {
        return rawContainer;
    }

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getImage() {
        return image.get();
    }

    public StringProperty imageProperty() {
        return image;
    }

    public void setImage(String image) {
        this.image.set(image);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public ObservableList<ContainerPort> getPorts() {
        return ports.get();
    }

    public ListProperty<ContainerPort> portsProperty() {
        return ports;
    }

    public void setPorts(ObservableList<ContainerPort> ports) {
        this.ports.set(ports);
    }
}
