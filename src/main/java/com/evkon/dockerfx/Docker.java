package com.evkon.dockerfx;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

public class Docker {
    private String host;
    private DockerClient client;

    public Docker(String host)
    {
        this.host = host;
        this.initClient();
    }

    public Docker()
    {
        this(System.getProperty("os.name").startsWith("Windows") ? "tcp://localhost:2375" : "unix:///var/run/docker.sock");
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getHost()
    {
        return this.host;
    }

    public void initClient()
    {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(this.host)
                .withApiVersion("1.23")
                .build();
        this.client = DockerClientBuilder.getInstance(config).build();
    }

    public DockerClient getClient()
    {
        return this.client;
    }
}
