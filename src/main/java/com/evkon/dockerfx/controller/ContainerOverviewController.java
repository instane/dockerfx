package com.evkon.dockerfx.controller;

import com.evkon.dockerfx.MainApp;
import com.evkon.dockerfx.model.Container;
import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.core.command.LogContainerResultCallback;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ContainerOverviewController {

    @FXML
    private ListView containersListView;

    @FXML
    private TextArea textArea;

    @FXML
    private Button docsButton;

    // Ссылка на главное приложение.
    private MainApp mainApp;

    /**
     * Конструктор.
     * Конструктор вызывается раньше метода initialize().
     */
    public ContainerOverviewController() {
    }

    /**
     * Инициализация класса-контроллера. Этот метод вызывается автоматически
     * после того, как fxml-файл будет загружен.
     */
    @FXML
    private void initialize() {
        textArea.setEditable(false);
        if (!Desktop.isDesktopSupported()) {
            docsButton.setDisable(true);
        }
    }

    /**
     * Вызывается главным приложением, которое даёт на себя ссылку.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        containersListView.setItems(mainApp.getContainerData());
    }

    private boolean isItemSelected() {
        try {
            this.getSelectedContainer();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @FXML
    public void listItemClick() throws InterruptedException {
        if (!isItemSelected()) {
            return;
        }
        Container container =  this.getSelectedContainer();

        textArea.setText("");
        class FrameReaderITestCallback extends LogContainerResultCallback {
            @Override
            public void onNext(Frame item) {
                textArea.appendText(item.toString());
                textArea.appendText("\n");
                super.onNext(item);
            }
        }

        FrameReaderITestCallback collectFramesCallback = new FrameReaderITestCallback();
        mainApp.getDocker().logContainerCmd(container.getId()).withStdOut(true).withStdErr(true)
                .withTailAll()
                .exec(collectFramesCallback).awaitCompletion();
    }

    private Container getSelectedContainer() {
        int index = containersListView.getSelectionModel().getSelectedIndex();
        return ((Container) containersListView.getItems().get(index));
    }

    @FXML
    public void stopClicked() {
        if (!isItemSelected()) {
            return;
        }
        Container container =  this.getSelectedContainer();
//        mainApp.getDocker().stopContainerCmd(container.getId()).exec();
        mainApp.getDocker().killContainerCmd(container.getId()).exec();
        textArea.setText("");
    }

    @FXML
    public void quitClicked() {
        mainApp.getPrimaryStage().close();
    }

    @FXML
    public void aboutClicked() {
        mainApp.showAboutAlert();
    }

    @FXML
    public void docsClicked() throws URISyntaxException, IOException {
        if (!isItemSelected()) {
            return;
        }
        Container container =  this.getSelectedContainer();
        Desktop.getDesktop().browse(
                new URI("http://hub.docker.com/r/" + container.getRawContainer().getImage())
        );
    }

    @FXML
    private void preferencesClicked() throws IOException {
        mainApp.showSettingsOverview();
    }
}
