package com.evkon.dockerfx.controller;

import com.evkon.dockerfx.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class SettingsController {
    private MainApp mainApp;

    @FXML
    private TextField hostField;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    public void setHostField(String host) {
        hostField.setText(host);
    }

    @FXML
    public void saveClicked() {
        mainApp.setDockerHost(hostField.getText());
        mainApp.initDocker();
        if (mainApp.isDockerInit()) {
            mainApp.closeSettingsOverview();
        }
    }
}
