package com.evkon.dockerfx;

import com.evkon.dockerfx.controller.ContainerOverviewController;
import com.evkon.dockerfx.controller.SettingsController;
import com.evkon.dockerfx.model.Container;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Version;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private DockerClient docker;
    private String dockerHost = "";
    private Stage settingsDialog;

    private ObservableList<Container> containerData = FXCollections.observableArrayList();
    public ObservableList<Container> getContainerData() {
        return containerData;
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("DockerFX");
        this.primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent e) {
                Platform.exit();
            }
        });

        initRootLayout();
        showContainerOverview();
        initDocker();
    }

    private void showDockerConnectionAlert()
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(this.getPrimaryStage());
        alert.setTitle("No Connection");
        if (dockerHost.length() > 0) {
            alert.setHeaderText("No Docker Connection at " + dockerHost);
        } else {
            alert.setHeaderText("No Docker Connection with defaults");
        }
        alert.setContentText("Please set a docker host in the settings.");
        alert.showAndWait();
    }

    public void showAboutAlert()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(this.getPrimaryStage());
        alert.setTitle("About");
        alert.setHeaderText("DockerFX v0.0.0.0.0.0.0.1");
        alert.setContentText("Made by @evkon");
        alert.showAndWait();
    }

    public void initDocker()
    {
        try {
            Docker docker = new Docker();
            if (dockerHost.length() > 0) {
                docker.setHost(dockerHost);
            }
            docker.initClient();
            this.docker = docker.getClient();
            final DockerClient client = docker.getClient();

            List<com.github.dockerjava.api.model.Container> containers = this.docker.listContainersCmd().exec();
            for (com.github.dockerjava.api.model.Container container : containers) {
                containerData.add(new Container(container));
            }

            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                public void run()
                {
                    Platform.runLater(new Runnable() {
                        public void run() {
                            ArrayList<String> naturalIDs = new ArrayList<String>();
                            List<com.github.dockerjava.api.model.Container> containers = client.listContainersCmd().exec();
                            for (com.github.dockerjava.api.model.Container _container : containers) {
                                Container container = new Container(_container);
                                naturalIDs.add(container.getId());
                                System.out.println(containerData.contains(container));

                                boolean found = false;
                                for (Container existedContainer: containerData
                                     ) {
                                    if (container.getId().equals(existedContainer.getId())) {
                                        existedContainer.setStatus(container.getStatus());
                                        found = true;
                                    }

                                }

                                if (!found) {
                                    containerData.add(container);
                                }
                            }
                            if (containers.size() < containerData.size()) {
                                int index = 0;
                                for (Container existedContainer: containerData) {
                                    if (!naturalIDs.contains(existedContainer.getId())) {
                                        containerData.remove(index);
                                    }
                                    ++index;
                                }
                            }
                        }
                    });
                }
            };
            timer.schedule(task, 0L ,1000L);
        } catch (Exception e) {
            showDockerConnectionAlert();
        }
    }

    public boolean isDockerInit() {
        Version version = docker.versionCmd().exec();
        return version.getApiVersion().length() > 0;
    }

    public void initRootLayout() {
        try {
            // Загружаем корневой макет из fxml файла.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Отображаем сцену, содержащую корневой макет.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showContainerOverview() {
        try {
            // Загружаем сведения об адресатах.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/ContainerOverview.fxml"));
//            AnchorPane containerOverview = (AnchorPane) loader.load();
            VBox containerOverview = (VBox) loader.load();

            // Помещаем сведения об адресатах в центр корневого макета.
            rootLayout.setCenter(containerOverview);

            // Даём контроллеру доступ к главному приложению.
            ContainerOverviewController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showSettingsOverview() throws IOException {
        Stage dialog = new Stage();
        this.settingsDialog = dialog;

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/SettingsOverview.fxml"));
        AnchorPane settingsOverview = (AnchorPane) loader.load();
        Scene scene = new Scene(settingsOverview);

        SettingsController controller = loader.getController();
        controller.setMainApp(this);
        controller.setHostField(dockerHost);

        dialog.setResizable(false);
        dialog.setScene(scene);
        dialog.initOwner(primaryStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.showAndWait();
    }

    public void closeSettingsOverview() {
        settingsDialog.close();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    public DockerClient getDocker() {
        return docker;
    }

    public void setDockerHost(String dockerHost) {
        this.dockerHost = dockerHost;
    }
}